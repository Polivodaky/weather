
import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nameCityLbl: UILabel!
    private let viewModel: ViewModel = ViewModel()
    private let disposeBag = DisposeBag()
    private let locationModel = LocationModel.shared
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationModel.locationManager.delegate = self
        locationModel.initLocation()
        
        tableView.register(UINib (nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "TableViewCell")
        tableView.delegate = self
        tableView.isHidden = true
        bind()
        subscribes()
    }
    
    func bind() {
        let cvReloadDataSource = RxTableViewSectionedReloadDataSource <WeatherItemsSection>(
            configureCell: { [unowned self] (tableView , cv, ip, item) in
                let cell = cv.dequeueReusableCell(withIdentifier: "TableViewCell", for: ip) as! TableViewCell
                cell.configure(item: item)
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                return cell
            }, canEditRowAtIndexPath: { _, _ in
                return true
        }, canMoveRowAtIndexPath: { _, _ in
            return true
        })
        
        viewModel.items
            .bind(to: tableView.rx.items(dataSource: cvReloadDataSource))
            .disposed(by: disposeBag)
    }
    
    func subscribes() {
        viewModel.cityNameIsReload.bind() { isReload in
            if isReload{
                DispatchQueue.main.async {
                    self.nameCityLbl.text = self.viewModel.getCity()
                    self.tableView.isHidden = false
                }
            }
        }.disposed(by: disposeBag)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
            
        self.viewModel.getDataWeather(locValue: userLocation.coordinate)
    }
        
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
}

extension ViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
}
