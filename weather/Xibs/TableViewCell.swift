
import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var minTempLbl: UILabel!
    @IBOutlet weak var maxTempLbl: UILabel!
    @IBOutlet weak var popLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configure(item: WeatherItemData) {
        dateLbl.text = item.date!
        minTempLbl.text = String(item.minTemp!) + "°C"
        maxTempLbl.text = String(item.maxTemp!) + "°C"
        popLbl.text = String(item.pop!) + "%"
    }
    
}
