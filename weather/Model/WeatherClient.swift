import Foundation
import CoreLocation

struct DataItem {
    var city: String?
    var weather: [WeatherItemData]
}

struct WeatherItemData {
    var date: String?
    var minTemp: Int?
    var maxTemp: Int?
    var pop: Int?
}

class WeatherClient {
    static let shared = WeatherClient()
    private var url = URL(string: "WEATHER_API")
    private var wetherID = "2f62c0354ab36fc33bd68fa654eaa08c"
    
    init() {
        
    }
    
    func fetchSummary(locValue: CLLocationCoordinate2D, completion: @escaping (DataItem) -> Void) {
        let stringURL = "https://api.openweathermap.org/data/2.5/onecall?lat=" + String(locValue.latitude) + "&lon=" + String(locValue.longitude) + "&exclude=&appid=" + wetherID + "&units=metric"
            
        url = URL(string: stringURL)
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            
            let parsedWeatherItems = self.setDataItem(data: data)
            completion(parsedWeatherItems)
        }.resume()
    }
    
    func setDataItem(data: Data?) -> DataItem{
        let weatherData = try? decoder.decode(WeatherData.self, from: data!)
        var itemData = DataItem(city: weatherData?.timezone, weather: [])
        
        weatherData?.daily?.forEach({ deily in
            if let date = deily.dt, let minTemp = deily.temp?.min, let maxTemp = deily.temp?.max, let pop = deily.pop {
                itemData.weather.append(WeatherItemData(date: Date(timeIntervalSince1970: date).getFormattedDate(format: "E, dd.MM"), minTemp: Int(minTemp), maxTemp: Int(maxTemp), pop: Int(pop * 100)))
            }
        })
        return itemData
    }
}


