
import Foundation

var decoder: JSONDecoder{
    let decode = JSONDecoder()
    decode.keyDecodingStrategy = .convertFromSnakeCase
    return decode
}

struct WeatherData: Decodable {
    var lat: Double?
    var lon: Double?
    var timezone: String?
    var timezone_offset: Int?
    var current: CurrentForecast?
    var daily: [DailyForecast]?
}

struct CurrentForecast: Decodable {
    var dt: Int?
    var temp: Float?
    var clouds: Float?
    var weather: [Weather]?
}

struct DailyForecast: Decodable {
    var dt: Double?
    var sunrise: Int?
    var sunset: Int?
    var moonrise: Int?
    var moonset: Int?
    var moon_phase: Float?
    var temp: Temperature?
    var feels_like: FeelsLike?
    var pressure: Int?
    var humidity: Int?
    var dew_point: Float?
    var wind_speed: Float?
    var wind_deg: Float?
    var weather: [Weather]?
    var clouds: Int?
    var pop: Float?
    var rain: Float?
    var uvi: Float?
}

struct Weather: Decodable {
    var id: Int?
    var main: String?
    var description: String?
    var icon: String?
}

struct Temperature: Decodable {
    var day: Float?
    var min: Float?
    var max : Float?
    var night: Float?
    var eve: Float?
    var morn: Float?
}

struct FeelsLike: Decodable {
    var day: Float?
    var night: Float?
    var eve: Float?
    var morn: Float?
}
