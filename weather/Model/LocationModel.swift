
import Foundation
import CoreLocation



class LocationModel {
    static let shared = LocationModel()
    
    let locationManager = CLLocationManager()
    private var locValue: CLLocationCoordinate2D?
    private var defaultLocValue: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 40.730610, longitude: -73.935242)
    private var placeMarkLoc : CLPlacemark?
    
    init() {
    }
    
    func initLocation() {
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
                
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
}


