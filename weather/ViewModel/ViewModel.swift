
import Foundation
import RxSwift
import RxCocoa
import RxDataSources
import CoreLocation


struct WeatherItemsSection {
    var items: [WeatherItemData]
}

extension WeatherItemsSection: SectionModelType {
    init(original: WeatherItemsSection, items: [WeatherItemData]) {
        self = original
        self.items = items
    }
}

class ViewModel {
    private let model: WeatherClient = WeatherClient.shared
    private let locationModel = LocationModel.shared
    private var weather: DataItem?
    
    var items = BehaviorRelay(value: [WeatherItemsSection]())
    var cityNameIsReload = BehaviorRelay<Bool>(value: false)
    
    init() {
        
    }
    
    func getDataWeather(locValue: CLLocationCoordinate2D) {
        model.fetchSummary(locValue: locValue, completion: { item in
            self.weather = item
            self.items.accept([WeatherItemsSection(items: item.weather)])
            self.cityNameIsReload.accept(true)
        })
    }
    
    func getCity() -> String {
        return weather?.city ?? ""
    }
}
